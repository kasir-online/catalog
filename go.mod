module catalog

go 1.16

require (
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/klauspost/compress v1.11.13 // indirect
	github.com/romnn/testcontainers v0.2.1
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/testcontainers/testcontainers-go v0.9.0
	go.mongodb.org/mongo-driver v1.5.2
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/text v0.3.6 // indirect
)
