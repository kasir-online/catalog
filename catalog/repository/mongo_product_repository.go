package repository

import (
	"catalog/domain"
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type mongoProductRepository struct {
	C *mongo.Collection
}

var _ domain.ProductRepository = (*mongoProductRepository)(nil)

func NewMongoProductRepository(collection *mongo.Collection) domain.ProductRepository {
	return &mongoProductRepository{C: collection}
}

func (p mongoProductRepository) GetAll(ctx context.Context) ([]domain.Product, error) {
	cur, err := p.C.Find(ctx, bson.D{})
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	var results []domain.Product

	for cur.Next(ctx) {
		if err = cur.Err(); err != nil {
			return nil, err
		}

		var elem domain.Product
		err = cur.Decode(&elem)
		if err != nil {
			return nil, err
		}
		results = append(results, elem)
	}
	return results, nil
}

var ErrNotFound = errors.New("not found")

func (p mongoProductRepository) GetByBarcodeID(ctx context.Context, barcodeID string) (domain.Product, error) {
	var product domain.Product
	filter := bson.D{{Key: "barcode_id", Value: barcodeID}}

	err := p.C.FindOne(ctx, filter).Decode(&product)

	if err == mongo.ErrNoDocuments {
		return product, ErrNotFound
	}

	return product, nil
}

func (p mongoProductRepository) Create(ctx context.Context, product *domain.Product) error {
	_, err := p.C.InsertOne(ctx, product)
	if err != nil {
		return err
	}
	return nil
}

func (p mongoProductRepository) Update(ctx context.Context, product *domain.Product) error {
	filter := bson.D{{Key: "key", Value: product.BarcodeID}}

	update := bson.D{{Key: "$set", Value: product}}
	_, err := p.C.UpdateOne(ctx, filter, update)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return ErrNotFound
		}
		return err
	}
	return nil
}

func (p mongoProductRepository) UpdateStock(ctx context.Context, stock *domain.Stock) error {
	product, err := p.GetByBarcodeID(ctx, stock.BarcodeID)
	if err != nil {
		return err
	}

	product.Stock = *stock

	err = p.Create(ctx, &product)

	if err != nil {
		return err
	}
	return nil
}

func (p mongoProductRepository) DisableProduct(ctx context.Context, barcodeID string) error {
	product, err := p.GetByBarcodeID(ctx, barcodeID)
	if err != nil {
		return err
	} else if !product.IsActive {
		return errors.New("product has been disabled")
	} else {
		product.IsActive = false
		_ = p.Update(ctx, &product)
	}
	return nil
}
