package repository

import (
	"catalog/domain"
	"context"
	"github.com/testcontainers/testcontainers-go"
	"reflect"
	"testing"

	"go.mongodb.org/mongo-driver/mongo"
)

func TestNewMongoProductRepository(t *testing.T) {
	type args struct {
		collection *mongo.Collection
	}
	tests := []struct {
		name string
		args args
		want domain.ProductRepository
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewMongoProductRepository(tt.args.collection); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMongoProductRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoProductRepository_GetAll(t *testing.T) {

	mongoC, collection, client := initDatabase()
	defer func(client *mongo.Client, ctx context.Context) {
		_ = client.Disconnect(ctx)
	}(client, context.Background())
	defer func(mongoC testcontainers.Container, ctx context.Context) {
		_ = mongoC.Terminate(ctx)
	}(mongoC, context.Background())

	domain1 := domain.Product{BarcodeID: "Test1"}
	domain2 := domain.Product{BarcodeID: "Test2"}

	var wantDomain []domain.Product
	wantDomain = append(wantDomain, domain1)
	wantDomain = append(wantDomain, domain2)

	var testData []interface{}
	testData = append(testData, domain1)
	testData = append(testData, domain2)

	_, err := collection.InsertMany(context.Background(), testData)

	if err != nil {
		t.Errorf("Error Inserting Data %v", err)
	}

	type fields struct {
		C *mongo.Collection
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []domain.Product
		wantErr bool
	}{
		{
			name:    "getAll",
			fields:  fields{C: collection},
			args:    args{ctx: context.Background()},
			want:    wantDomain,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := mongoProductRepository{
				C: tt.fields.C,
			}
			got, err := p.GetAll(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("mongoProductRepository.GetAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("mongoProductRepository.GetAll() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoProductRepository_GetByBarcodeID(t *testing.T) {
	type fields struct {
		C *mongo.Collection
	}
	type args struct {
		ctx       context.Context
		barcodeID string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    domain.Product
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := mongoProductRepository{
				C: tt.fields.C,
			}
			got, err := p.GetByBarcodeID(tt.args.ctx, tt.args.barcodeID)
			if (err != nil) != tt.wantErr {
				t.Errorf("mongoProductRepository.GetByBarcodeID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("mongoProductRepository.GetByBarcodeID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoProductRepository_Create(t *testing.T) {
	type fields struct {
		C *mongo.Collection
	}
	type args struct {
		ctx     context.Context
		product *domain.Product
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := mongoProductRepository{
				C: tt.fields.C,
			}
			if err := p.Create(tt.args.ctx, tt.args.product); (err != nil) != tt.wantErr {
				t.Errorf("mongoProductRepository.Create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_mongoProductRepository_Update(t *testing.T) {
	type fields struct {
		C *mongo.Collection
	}
	type args struct {
		ctx     context.Context
		product *domain.Product
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := mongoProductRepository{
				C: tt.fields.C,
			}
			if err := p.Update(tt.args.ctx, tt.args.product); (err != nil) != tt.wantErr {
				t.Errorf("mongoProductRepository.Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_mongoProductRepository_UpdateStock(t *testing.T) {
	type fields struct {
		C *mongo.Collection
	}
	type args struct {
		ctx   context.Context
		stock *domain.Stock
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := mongoProductRepository{
				C: tt.fields.C,
			}
			if err := p.UpdateStock(tt.args.ctx, tt.args.stock); (err != nil) != tt.wantErr {
				t.Errorf("mongoProductRepository.UpdateStock() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_mongoProductRepository_DisableProduct(t *testing.T) {
	type fields struct {
		C *mongo.Collection
	}
	type args struct {
		ctx       context.Context
		barcodeID string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := mongoProductRepository{
				C: tt.fields.C,
			}
			if err := p.DisableProduct(tt.args.ctx, tt.args.barcodeID); (err != nil) != tt.wantErr {
				t.Errorf("mongoProductRepository.DisableProduct() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
