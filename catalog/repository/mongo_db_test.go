package repository

import (
	"context"
	"fmt"
	"time"

	tc "github.com/romnn/testcontainers"
	tcmongo "github.com/romnn/testcontainers/mongo"
	log "github.com/sirupsen/logrus"
	"github.com/testcontainers/testcontainers-go"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func initDatabase() (testcontainers.Container, *mongo.Collection, *mongo.Client) {

	log.SetLevel(log.InfoLevel)
	var mongoC testcontainers.Container
	// Start mongo container
	mongoC, mongoCfg, err := tcmongo.StartMongoContainer(context.Background(), tcmongo.ContainerOptions{
		ContainerOptions: tc.ContainerOptions{CollectLogs: true},
	})
	if err != nil {
		log.Fatalf("Failed to start mongoDB container: %v", err)
	}
	var containerLog string
	if mongoCfg.Log != nil {
		go func() {
			for {
				msg := <-mongoCfg.Log.MessageChan
				containerLog = containerLog + msg
			}
		}()
		log.Infof("Collecting container logs for the next 15 seconds")
		time.Sleep(15 * time.Second)
		defer fmt.Println(containerLog)
	}

	// Connect to the database
	mongoURI := mongoCfg.ConnectionURI()

	client, err := mongo.NewClient(options.Client().ApplyURI(mongoURI))
	if err != nil {
		log.Fatalf("Failed to create mongo client (%s): %v", mongoURI, err)
	}
	mctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	_ = client.Connect(mctx)

	err = client.Ping(mctx, readpref.Primary())
	if err != nil {
		log.Fatalf("Could not ping database within %d seconds (%s): %v", 20, mongoURI, err)
	}
	database := client.Database("testdatabase")
	collection := database.Collection("my-collection")

	return mongoC, collection, client
}
