package domain

import (
	"context"
	"time"
)

type Product struct {
	BarcodeID string    `json:"barcode_id"`
	Name      string    `json:"name"`
	Price     string    `json:"price"`
	CreatedAt time.Time `bson:"created_at" json:"created_at,omitempty"`
	CreatedBy string    `json:"created_by"`
	UpdatedAt time.Time `bson:"updated_at" json:"updated_at,omitempty"`
	UpdatedBy string    `json:"updated_by"`
	Stock     Stock     `json:"stock"`
	IsActive  bool      `json:"is_active"`
}

type ProductRepository interface {
	GetAll(ctx context.Context) ([]Product, error)
	GetByBarcodeID(ctx context.Context, barcodeID string) (Product, error)
	Create(ctx context.Context, product *Product) error
	Update(ctx context.Context, product *Product) error
	UpdateStock(ctx context.Context, stock *Stock) error
	DisableProduct(ctx context.Context, barcodeID string) error
}
