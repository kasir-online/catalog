package domain

import "time"

type Stock struct {
	BarcodeID      string    `json:"barcode_id"`
	AvailableStock int64     `json:"available_stock"`
	CreatedAt      time.Time `bson:"created_at" json:"created_at,omitempty"`
	CreatedBy      string    `json:"created_by"`
	UpdatedAt      time.Time `bson:"updated_at" json:"updated_at,omitempty"`
	UpdatedBy      string    `json:"updated_by"`
}
